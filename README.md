## Test Flask web framework
## Screenshot

![alt text](doc/preview.png "Flask")

## Create DB
```
python manage.py db init
python manage.py db migrate
python manage.py db upgrade
```

## Environment usage
DATABASE_URL = postgresql://postgres:toor@localhost:5432/test_flask
APP_SETTINGS = config.DevelopmentConfig
