import os
## default config
class BaseConfig(object):
	DEBUG = True
	SECRET_KEY = 'test key'
	SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
	# SQLALCHEMY_DATABASE_URI = 'sqlite:///db.sqlite'
	SQLALCHEMY_TRACK_MODIFICATIONS = False

class DevelopmentConfig(BaseConfig):
	DEBUG = True

class ProductionConfig(BaseConfig):
	DEBUG = False
	SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
	# SQLALCHEMY_DATABASE_URI = 'postgresql-amorphous-77129'
	
