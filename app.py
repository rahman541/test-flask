from flask import Flask, render_template, request, redirect, url_for, session, flash
from flask_mysqldb import MySQL
from functools import wraps
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt

app = Flask(__name__)
bcrypt = Bcrypt(app)

# Config
import os
app.config.from_object(os.environ['APP_SETTINGS'])

## SQLAlchemy
db = SQLAlchemy(app)

# SQLAlchemy model
from models import *

## Decorator
def login_required(f):
	@wraps(f)
	def wrap(*args, **kwargs):
		if 'logged_in' in session:
			return f(*args, **kwargs)
		else:
			flash('You need to login first.')
			return redirect(url_for('login'))
	return wrap

## Mysql Config
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'test_flask'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
mysql = MySQL(app)

@app.route("/")
@login_required
def home():
	posts = db.session.query(BlogPost).all()
	print (posts)
	return render_template('home.html', posts=posts)
@app.route("/about")
def about():
	return render_template('about.html')
@app.route("/login", methods=['GET','POST'])
def login():
	error = None
	if request.method == 'POST':
		if request.form['email'] != 'admin@gmail.com':
			error = 'Invalid credential'
		else:
			session['logged_in'] = True
			flash('Successfully login')
			return redirect(url_for('home'))
	return render_template('login.html', error=error)
@app.route('/logout')
def logout():
	session.pop('logged_in', None)
	flash('You were logged out.')
	return redirect(url_for('home'))
@app.route('/user/<string:id>')
def user(id):
	curr = mysql.connection.cursor()
	result = curr.execute('SELECT * FROM users WHERE id=%s', [id])
	if result > 0:
		user = curr.fetchone()
		return 'User: ' + user['name']
	return 'User not found'

if __name__ == "__main__":
	app.run()
